function AlterMSH(MSH) 
   MSH[3][1] = 'MyApp'
   MSH[4][1] = 'MyOffice'
   return MSH
end

function AlterPID(PID)
   PID[5][1][2] = PID[5][1][2]:nodeValue():upper()
   PID[5][1][1][1] = PID[5][1][1][1]:nodeValue():upper()
   PID[13][1][1] = phone.clean_phone_us(PID[13][1][1])
	local DOB = dateparse.parse(PID[7][1]:nodeValue())
   PID[7][1] = os.date('%Y%m%d',DOB)
   return PID
end

function AddNote(Out)
   local Lines = NoteTemplate:split('\n')
   for i=1, #Lines do 
      Out.NTE[i][1]    = i 
      Out.NTE[i][3][1] = Lines[i]
   end
   return Out   
end

NoteTemplate=[[
Positive attitude is now known to be a root cause of many positive life benefits.
A positive attitude is the inclination towards a hopeful state of mind.
Your way of thinking, whether positive or negative, is a habit.
Let go of the assumption that the world is against you.
Understand that the past does not equal the future. 
See yourself as a cause, not an effect. 
Use positive affirmations.
Remember that life is short.
Be a balanced optimist.
]]
