-- The main function is the first function called from Iguana.
-- The Data argument will contain the message to be processed.
require 'mappings'
require 'vip'

require 'cleanphone'
require 'dateparse'
require 'split'

function main(Data)

   -- PARSING
   local OrigMsg, MsgType, Errors = hl7.parse{vmd="demo.vmd",data=Data}

   -- ALERTING
   CheckForVIP(OrigMsg)

   -- FILTERING
   if MsgType == 'Catchall' then 
      iguana.logInfo('This message was filtered '..OrigMsg.MSH[9][1]..' '..OrigMsg.MSH[9][2]) 
      return
   end
   
   -- MAPPING
   local Out = hl7.message{vmd='demo.vmd', name=MsgType}
      
   Out:mapTree(OrigMsg)
   
   AlterMSH(Out.MSH)

   -- CONDITIONAL MAPPING & PROCESSING
   if MsgType == 'Lab' then 
      AlterPID(Out.PATIENT.PID)
   elseif MsgType == 'ADT' then 
      AlterPID(Out.PID)
   end
   
   if MsgType == 'Lab' then
      AddNote(Out)  
   end
   
   trace(Out)

   
   -- SEND IT TO MY DESTINATION!
   local DataOut = tostring(Out)
   queue.push{data=DataOut}

end






