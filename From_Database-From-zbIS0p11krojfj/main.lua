-- The main function is the first function called from Iguana.
require 'node'

conn = db.connect{api=db.SQLITE, name='test.sqlite'}

function main()
   local R = conn:query{sql='SELECT Id FROM Patient WHERE Updated = 1'}

   for i=1, #R do 
      PushId(R[i].Id:nodeValue())
   end

end

function PushId(Id)
   queue.push{data=Id}
end