function MapPID(PID, T)
   PID[3][1][1]    = T.Id
   PID[5][1][1][1] = T.LastName
   PID[5][1][2]    = T.GivenName
   PID[7]          = T.Dob
   PID[8]          = T.Sex
   PID[10][1][1]   = T.Race
   PID[13][1][1]   = T.PhoneHome
   PID[14][1][1]   = T.PhoneBusiness
   PID[16][1]      = T.MaritalStatus
   PID[17][1]      = T.Religion
   PID[20][1]      = T.LicenseNumber
   return PID
end

function MapMSH(MSH)
   MSH[3][1] = 'Iguana'
   MSH[4][1] = 'Training'
   MSH[7][1] = os.date('%Y%m%d%H%M%S')
   MSH[9][1] = 'ADT'
   MSH[9][2] = 'A08'
   MSH[11][1]= 'P'
   MSH[12][1]= '2.7'
   MSH[20]   = ''
   return MSH

end