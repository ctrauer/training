-- ## Import shared logic & common toolsets
require 'mappings'
local file = require 'embedBinary'



-- ## Troubleshoot in one place using trace function
-- local function trace(A) return end

-- ## Define database connection
conn = db.connect{api=db.SQLITE,name='test.sqlite'}



-- The main function is the first function called from Iguana.
-- The Data argument will contain the message to be processed.
function main(Id)

-- ## Define database connection
--conn = db.connect{api=db.SQLITE,name='test.sqlite'}
   
   
   -- Retrieve record
	local Patient = conn:query{sql='SELECT * FROM Patient WHERE Id = "' .. Id .. '"'}
   
   
   -- ## Define outbound message
   local Msg = hl7.message{vmd='hl7.vmd',name='ADT'}
   
   
   -- ## Execute mappings and transformations
   MapMSH(Msg.MSH)
   MapPID(Msg.PID, Patient[1])
 
   -- ## Embed binary processes
   
   -- 1. Read in file (file path is hardcoded in this instance)
   local pdf = file.readBinary()
   
   -- 2. Encrypt in base64
   pdf = filter.base64.enc(pdf)
   
   -- 3. Map to message
   Msg.MSH[5][3] = pdf
   
   trace(Msg)
   
   
   -- 4. Writing binary files to disk
   --file.writeBinary(filter.base64.dec(pdf))
   
   
   
   trace(Msg)
   trace(Msg.PID[5][1][1][1]:nodeValue())

   -- ## Send data
   queue.push{data=tostring(Msg)}
   
   -- Reset record processing flag in database
   -- so record doesn't get retrieve again
   conn:execute{sql=[[UPDATE patient SET Updated = 0 WHERE Id = ']]
      .. Id .. [[']], live = true}
   
end

