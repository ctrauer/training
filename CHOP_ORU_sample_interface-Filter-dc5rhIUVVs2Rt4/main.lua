-- The main function is the first function called from Iguana.
-- The Data argument will contain the message to be processed.
-- import
require 'TransformOBX'

function main(Data)

   -- Parse incoming message
   local Orig, Msg, Err = hl7.parse{vmd='chop_lab_result.vmd',data=Data}

   -- Create outbound message
   local Out = hl7.message{vmd='chop_lab_result.vmd',name='Lab_Result'}

   -- Map inbound to outbound message
   Out:mapTree(Orig)
   trace(Out)

   -- Transform identifier values
   ProcessOBX(Orig.OBX,Out)

   -- Filter out segments we don't want
   FilterOBX(Out)

   -- Renumber OBX segments
   ReorderOBX(Out)

   -- Check our output in annotations
   trace(Out)

   -- Convert message back to string and
   -- queue to next component (or destination)
   queue.push{data=Out:S()}


end







