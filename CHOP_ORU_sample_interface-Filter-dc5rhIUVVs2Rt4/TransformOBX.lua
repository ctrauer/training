function ProcessOBX(Orig,Out) 

   local artsNum = 0
   trace(O)
   
   -- Find segments that contain ART in OBX.5.1   
   for i=1, #Orig do 


      trace(Orig[i][1]:nodeValue())
      if Orig[i][5][1]:nodeValue() == 'ART' then

         -- Splits identifier by hyphens
         local id = Orig[i][3][1]:nodeValue():split('-')

         trace(id)

         artsNum = artsNum + 1
         -- Replaces ID value
         TransformIDs(Out, id, artsNum)

      end

   end

end


function TransformIDs(Out,id,num) 

   -- Cycle through OBX segments and
   -- transform the ART-related segments.
   for j=1, #Out.OBX do 

      -- Splits identifier by hyphens
      local identifier = Out.OBX[j][3][1]:nodeValue():split('-')

      -- Compared current identifier with the ART source ID
      if identifier[2] == id[2] 
         and identifier[3] == id[3] then 

         -- replace identifier with ART and iterator
         Out.OBX[j][3][1] = id[1] .. '-' .. 
         'ART' .. num .. '-' .. identifier[4]

      end

   end

   return Out  
end

function FilterOBX(Out) 

   -- Cycle through OBX segments and
   -- take out ones we don't want.
   for i=#Out.OBX,1,-1 do 

      local identifier = Out.OBX[i][3][1]:nodeValue():split('-')

      if identifier[2] == 'ART' and identifier[3] == 'HR' then
         Out.OBX:remove(i)

      elseif Out.OBX[i][2]:nodeValue() == 'ST' then
         Out.OBX:remove(i)

         -- Remove duplicate ARTs
      elseif identifier[2] == 'ART' then
         Out.OBX:remove(i)
      end


   end
   return Out  

end

function ReorderOBX(Out) 

   -- Cycle through OBX segments and
   -- renumber them.
   for i=1, #Out.OBX do 

      Out.OBX[i][1] = i

   end


   return Out  


end