require 'dateparse'
require 'retry'
local mapping = require 'mapping'
local database = require 'database'


-- # DEFINE DATABASE CONNECTION
conn = db.connect{api=db.SQLITE,name='Training/test.sqlite'}

function trace(A) return end


function main(Data)
   
   -- ## PARSE MESSAGE
   local Msg, Name, Errors = hl7.parse{vmd='hl7.vmd',data=Data}

	-- ## CREATE STAGING TABLE
   -- There are two methods we can show.
	-- The traditional method uses VMDS:
   local T = db.tables{vmd='tables.vmd',name='Message'}
   trace(T)
   
	-- The Iguana-only method uses DBS files:
   local F = dbs.init{filename='tables.dbs'}
   local T2 = F:tables()
   trace (T2)
   
	-- # POPULATE DB STAGING TABLE
   mapping.MapData(Msg, Name, T)
   trace(T)

	-- # UPDATE DATABASE   
   conn:merge{data=T,live=true}

   -- # VIEW PATIENT TABLE
   local success, rows = pcall(database.browseDB,'Patient')
   trace(success)
   trace(rows)
   if not success then database.errHandler(rows) end

end














