local database = {}

function database.browseDB(table) 
   local rows = conn:query{sql='SELECT * FROM ' .. table}
   return rows
end

function database.errHandler(err) 
   iguana.logError(err.message .. ' (' .. err.code .. ')')
end

trace(database)


return database