-- ## Add required modules needed to support interface
local vip = require 'logibec.db.connect'
local queries = require 'logibec.db.queries'
local mappings = require 'logibec.db.mappings'
--local endpoints = require 'logibec.ws.connect'
local mappings2 = require 'logibec.ws.mappings'
local queries2 = require 'logibec.ws.queries'
-- Extend VIP namespace to add more functions. Useful for popup menus!
vip.db = {}
vip.db.queries = queries
vip.db.mappings = mappings
vip.ws = {}
vip.ws.mappings = mappings2
vip.ws.queries = queries2


local commands = {}

function commands.CreateEmployee(empRecord) 

   -- Map to outbound JSON object
   local RecordOut = vip.ws.mappings.mapEmpRecord(empRecord)
   trace(RecordOut)

   -- Append command
   RecordOut.commands[1].type = 'CreateEmployee'

   -- Append externalCopy (in this case, a duplicate of data)
   RecordOut.externalCopy = RecordOut.commands[1].data
   
   -- Queue event message
   queue.push{data=json.serialize{data=RecordOut}}

   iguana.logInfo("CreateEmployee event message for " .. RecordOut.commands[1].data.Id ..
      ": " .. json.serialize{data=RecordOut})
end



function commands.CreatePhoneNumber(empRecord,phoneType) 

   -- ## Query DB for phone record

   local phoneRecord

   if phoneType == 'home' then 
      phoneRecord = conn:query{sql=vip.db.queries.getEmpPhoneHome(empRecord.Id)}

   elseif phoneType == 'mobile' then 
      phoneRecord = conn:query{sql=vip.db.queries.getEmpPhoneMobile(empRecord.Id)}

   else 
      iguana.error('phoneType must be "home" or "mobile"') 
   end

   phoneRecord = json.parse{data=phoneRecord[1].JSON:nodeValue()}

   -- Map to outbound JSON object
   local RecordOut = vip.ws.mappings.mapEmpPhone(phoneRecord)
   trace(RecordOut)

   -- Append command
   RecordOut.commands[1].type = 'CreatePhoneNumber'
   
   -- Append externalCopy (in this case, a duplicate of data)
   RecordOut.externalCopy = RecordOut.commands[1].data

   -- Queue event message
   queue.push{data=json.serialize{data=RecordOut}}

   iguana.logInfo("CreatePhoneNumber (" .. phoneType .. ") event message for " .. RecordOut.commands[1].data.Id ..
      ": " .. json.serialize{data=RecordOut})

end




function commands.ModifyEmployee(empRecord,extCopy) 

   -- Map to outbound JSON object
   local RecordOut = vip.ws.mappings.mapEmpRecord(empRecord)
   trace(RecordOut)

   -- Append command
   RecordOut.commands[1].type = 'ModifyEmployee'

   -- Append externalCopy
   RecordOut.externalCopy = json.parse{data=extCopy}

   trace(RecordOut)

   -- Queue event message
   queue.push{data=json.serialize{data=RecordOut}}

   iguana.logInfo("ModifyEmployee event message for " .. RecordOut.commands[1].data.Id ..
      ": " .. json.serialize{data=RecordOut})
end





function commands.UpdatePhoneNumber(empRecord,phoneType) 

   -- ## Query DB for phone record

   local phoneRecord

   if phoneType == 'home' then 
      phoneRecord = conn:query{sql=vip.db.queries.getEmpPhoneHome(empRecord.Id)}

   elseif phoneType == 'mobile' then 
      phoneRecord = conn:query{sql=vip.db.queries.getEmpPhoneMobile(empRecord.Id)}

   else 
      iguana.error('phoneType must be "home" or "mobile"') 
   end

   phoneRecord = json.parse{data=phoneRecord[1].JSON:nodeValue()}

   -- Map to outbound JSON object
   local RecordOut = vip.ws.mappings.mapEmpPhone(phoneRecord)
   trace(RecordOut)


   -- ## Queries externalCopy web service
   local extCopyPhone = json.parse{data=vip.ws.queries.getExtCopyPhone(phoneRecord.Id)}




   -- ## Test DB Version against externalCopy

   -- if Tel is null in DB but not in external copy ... DELETE
   if (phoneRecord.number == "" and extCopyPhone.number ~= '') then 
      trace(true)
      -- Append command
      RecordOut.commands[1].type = 'DeletePhoneNumber'

      -- Append externalCopy
      RecordOut.externalCopy = extCopyPhone

      trace(RecordOut)

   -- Queue event message
   queue.push{data=json.serialize{data=RecordOut}}


      iguana.logInfo("DeletePhoneNumber event message for " .. RecordOut.commands[1].data.Id ..
         ": " .. json.serialize{data=RecordOut})   


      -- if Tel is not null in DB but does not exist in external copy .. CREATE
   elseif (phoneRecord.number ~= '' and extCopyPhone.number == '') then
      trace(true)
      -- Append command
      RecordOut.commands[1].type = 'CreatePhoneNumber'

      -- Append externalCopy (in this case, a duplicate of data)
      RecordOut.externalCopy = RecordOut.commands[1].data

      trace(RecordOut)

   -- Queue event message
   queue.push{data=json.serialize{data=RecordOut}}


      iguana.logInfo("CreatePhoneNumber event message for " .. RecordOut.commands[1].data.Id ..
         ": " .. json.serialize{data=RecordOut})


      -- if Tel is not null in DB but does not match external copy .. MODIFY
   elseif (phoneRecord.number ~= '' and extCopyPhone.number ~= phoneRecord.number) then
      trace(true)

      -- Append command
      RecordOut.commands[1].type = 'ModifyPhoneNumber'

      -- Append externalCopy
      RecordOut.externalCopy = extCopyPhone

      trace(RecordOut)

   -- Queue event message
   queue.push{data=json.serialize{data=RecordOut}}


      iguana.logInfo("ModifyPhoneNumber event message for " .. RecordOut.commands[1].data.Id ..
         ": " .. json.serialize{data=RecordOut})

   else

      -- Do nothing. Records probably match, so no update needed.
      return
   end
end




return commands