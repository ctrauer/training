-- The main function is the first function called from Iguana.
-- The Data argument will contain the message to be processed.

-- ## Add required modules needed to support interface
local vip = require 'logibec.db.connect'
local queries = require 'logibec.db.queries'
local mappings = require 'logibec.db.mappings'
--local endpoints = require 'logibec.ws.connect'
local mappings2 = require 'logibec.ws.mappings'
local queries2 = require 'logibec.ws.queries'
-- Extend VIP namespace to add more functions. Useful for popup menus!
vip.db = {}
vip.db.queries = queries
vip.db.mappings = mappings
vip.ws = {}
vip.ws.mappings = mappings2
vip.ws.queries = queries2

local commands = require 'commands'



-- ## Define database connection object
local conn = vip.init()


function main(Data)

   -- ## Parse queued record back to JSON
   local recordIn = json.parse{data=Data}

   -- ## Query DB for full employee record
   local empRecord = conn:query{sql=vip.db.queries.getEmpRecord(recordIn.sourceid)}

   -- Extract JSON from DB result set and parse it
   empRecord = json.parse{data=empRecord[1].JSON:nodeValue()}

   -- ## Query externalCopy for pre-existing employee record

   -- Ideally we would cache this token, but we'll just get it every time for PoC
   token = vip.ws.queries.getToken()

   -- Queries externalCopy web service
   local extCopyEmp = vip.ws.queries.getExtCopyEmp(empRecord.Id,token)


   -- ## Based on result, decide what kind of 
   --    messages/queries to send to web services
   --    for employee records and queue the message out.

   if extCopyEmp == '' then

      commands.CreateEmployee(empRecord)
      commands.CreatePhoneNumber(empRecord,'home')
      commands.CreatePhoneNumber(empRecord,'mobile')

   else 
      commands.ModifyEmployee(empRecord,extCopyEmp)
      commands.UpdatePhoneNumber(empRecord,'home')
      commands.UpdatePhoneNumber(empRecord,'mobile')
   end

end