local mapping = {}


function mapping.MapData(Msg, Name, T) 

   if Name == 'Lab' then 
      ProcessLab(T, Msg)
   elseif Name == 'ADT' then
      ProcessADT(T, Msg)
   end

   return FF

end


function ProcessADT(T, Msg) 
   MapPatient(T.Patient[1],Msg.PID)
end


function ProcessLab(T, Msg) 


end


function MapPatient(T, PID) 
   T.Id = PID[3][1][1]:nodeValue()
   T.LastName = PID[5][1][1][1]:nodeValue()
   T.GivenName = PID[5][1][2]:nodeValue()
   T.Sex = PID[8]:nodeValue()
   T.Ssn = PID[19]:nodeValue()
   T.Dob = PID[7]:D()
   T.Updated = 1
   return T
end


return mapping