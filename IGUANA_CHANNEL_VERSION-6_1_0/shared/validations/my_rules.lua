local validations = {}

-- Define the outgoing message format. In this case, it's JSON.
function validations.logMsgErrors(Err) 
  trace(#Err)
   for i=1, #Err do
      iguana.logInfo(Err[i].description)   
      end  
end



return validations