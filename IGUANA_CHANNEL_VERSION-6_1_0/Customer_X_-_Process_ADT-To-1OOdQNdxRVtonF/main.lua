-- The main function is the first function called from Iguana.
-- The Data argument will contain the message to be processed.
function main(Data)

   
   -- Submit request to web service
   
local response, status, headers = 
   net.http.post{url='http://target.interfaceware.com:6544/tutorial', body = Data, live=true}
   

   -- Handle the response

   if status == 200 then
      iguana.logDebug("Request successful: " .. response .. " (" .. status .. ")")    
   else
      iguana.logDebug("Request unsuccessful: " .. response .. " (" .. status .. ")")      
   end

   
   
end