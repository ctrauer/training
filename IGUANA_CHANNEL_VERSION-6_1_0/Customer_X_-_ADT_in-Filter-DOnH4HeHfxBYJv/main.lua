-- Import shared logic
local mappings = require 'mappings.cerner.ADT.2_3_1'
local myDataModel = require 'mappings.my_data_model'
local filters = require 'validations.my_filters'
local validations = require 'validations.my_rules'
require 'cleanphone'


function main(Data)
      
   -- Parse our incoming HL7 message
   local MsgIn, MsgType, Error = hl7.parse{vmd='demo.vmd',data=Data}
   
   
   -- Filter this message?
   local filterTest = filters.checkFilterRules(MsgType)
 
   if filterTest == false then
      trace(filterTest)
      return -- stop processing this message
   end


   -- Log all HL7 schema validation messages
   validations.logMsgErrors(Error)
   
 
   -- Create outbound message
   local MsgOut = myDataModel.buildOutboundMsg(MsgIn,myDataModel.template(),mappings)
   trace(MsgOut)
	
   
   -- #### LOCAL MAPPINGS #### --
   trace(MsgOut.patient.phone_home, MsgOut.patient.phone_work)
   phone.clean_phone_us(MsgOut.patient.phone_home)
   phone.clean_phone_us(MsgOut.patient.phone_work)

   
   -- Send to message queue
   queue.push{data=MsgOut}
   
end

   