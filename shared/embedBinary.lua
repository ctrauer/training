local file = {}


function file.readBinary()
   local INPUT_DIR = iguana.workingDir()
   local BINARY_DIR = 'PDF/'
   FILE_PATTERN = 'sample.pdf' 
   
 
   local P = io.open(INPUT_DIR..BINARY_DIR..FILE_PATTERN)
   local file = P:read('*a')

   return file
end
 
function file.showMeDecryption(s)
   return filter.base64.dec(s)
end


function file.showMeEncryption(s)
   return filter.base64.enc(s)
end
 
function file.writeBinary(binData)
   local INPUT_DIR = iguana.workingDir()
   local BINARY_DIR = 'PDF/'
   FILE_PATTERN = 'sample_output.pdf' 
 
   local fn=INPUT_DIR..BINARY_DIR..FILE_PATTERN  
   local f = io.open(fn, "wb")
   f:write(binData)
   f:close()   
end


return file