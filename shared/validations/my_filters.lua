local filters = {}

-- Define the outgoing message format. In this case, it's JSON.
function filters.checkFilterRules(MsgType) 
   if MsgType ~= 'ADT' then
      print('Discarding ' .. MsgType .. ' message')
      return false
   else return true
   end   
end



return filters