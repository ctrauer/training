local mappings = {}

function mappings.empRecord () 

   empRecord = [[{
   "correlationId": "",
   "commands": [{
   "data":
   {
   "Version":"1",
   "Id":"",
   "Code":"",
   "firstName":"",
   "middleName":"",
   "lastName":"",
   "gender":"",
   "hireDt":"",
   "terminationDt":"null",
   "salutationId":"",
   "employeeCategoryId":""
},
   "type": ""
}],
   "type": "employee",
   "externalCopy":""}]]

   return json.parse(empRecord)
end 


function mappings.empPhone () 

   empPhone = [[{
   "correlationId": "",
   "commands": [{
   "data":
   {
"extension": "",
"employeeId": "",
"customLabel": "",
"number": "",
"label": "",
"Id": "",
"Version": 1
},
   "type": ""
}],
   "type": "PhoneNumber",
   "externalCopy":""
}]]

   return json.parse(empPhone)
end 

function mappings.externalCopy() 
extCopy = [[ {
   "Version":"1",
   "Id":"",
   "Code":"",
   "firstName":"",
   "middleName":"",
   "lastName":"",
   "gender":"",
   "hireDt":"",
   "terminationDt":"",
   "salutationId":"",
   "employeeCategoryId":""
}
   ]]
return extCopy
end

function mappings.mapEmpRecord(recordIn)

   local recordOut = mappings.empRecord()
   
   recordOut.correlationId = recordIn.correlationId
   recordOut.commands[1].data.Id = recordIn.Code
   recordOut.commands[1].data.Code = recordIn.Code
	recordOut.commands[1].data.firstName = recordIn.firstName
   recordOut.commands[1].data.middleName = recordIn.middleName
   recordOut.commands[1].data.lastName = recordIn.lastName
   recordOut.commands[1].data.hireDt = recordIn.hireDt
   recordOut.commands[1].data.gender = recordIn.gender
   recordOut.commands[1].data.terminationDt = recordIn.terminationDt
   recordOut.commands[1].data.salutationId = recordIn.salutationId
   recordOut.commands[1].data.employeeCategoryId = recordIn.employeeCategoryId
   
   return recordOut
end

function mappings.mapEmpPhone(recordIn)

   local recordOut = mappings.empPhone()
   
   recordOut.correlationId = recordIn.correlationId
   recordOut.commands[1].data.extension = recordIn.extension
   recordOut.commands[1].data.employeeId = recordIn.employeeId
   recordOut.commands[1].data.customLabel = recordIn.customLabel
   recordOut.commands[1].data.number = recordIn.number
   recordOut.commands[1].data.label = recordIn.label
   recordOut.commands[1].data.Id = recordIn.Id
   recordOut.commands[1].data.Version = recordIn.Version
   return recordOut
end

return mappings