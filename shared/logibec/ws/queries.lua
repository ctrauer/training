local queries = {}


function queries.getToken() 

   local uri = "https://ids-srv-int.logibec.com/core/connect/token"
   local scope = "hrpayroll-api"
   local clientid = "hrpayroll-nbd20a"
   local secret = "hcmwillbegreat"

   local body = "grant_type=client_credentials&scope=" .. scope .. "&client_id=".. clientid .."&client_secret=".. secret
   trace(body)

   local headers = {['Content-Type']='application/x-www-form-urlencoded'}
	local method = 'post'
   
   local status, response = pcall(wsQuery,method,uri,headers,body)


   trace(status)
   trace(response)



   if status then 
      local token = json.parse{data=response}
      return token.access_token
   else 
      local token = [[{"access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IkpFNERyREhZNERTQ1B4YmJzN0k4ekdUaExYOCIsImtpZCI6IkpFNERyREhZNERTQ1B4YmJzN0k4ekdUaExYOCJ9.eyJpc3MiOiJodHRwczovL3c1bmlnaHRiMTcwMS5sb2dpYmVjLmNvbS8iLCJhdWQiOiJodHRwczovL3c1bmlnaHRiMTcwMS5sb2dpYmVjLmNvbS9yZXNvdXJjZXMiLCJleHAiOjE0ODk3OTEwNzksIm5iZiI6MTQ4OTc4NzQ3OSwiY2xpZW50X2lkIjoiaHJwYXlyb2xsLW5iZDIwYSIsInNjb3BlIjoiaHJwYXlyb2xsLWFwaSJ9.s1YCCbbDsrV_eCjArrYv1XdzS_sluzO1koQsBr0kWCFZgXs9z8KUaoc68LCCC1JKBk_44cP8y-MC5ATzR_T-BPRq8PLIUq2E3fHlCQtUbdZFXWRl_3h4qk1_7s9hfXSCH48mB6r1Iw2DJLsk5nOeoHS5LPLG36shn4PFIqC_zwKHiydtWWdLyE6Sb2kj0hXvpNAn5IXf8DvyN4NZTAQ3mOABeUb46PpfsOUmBMoHgZFfTrz_dMc7W1pszEIUrfy6D9JNyZOOlu9DadWBtqZVEXds5jeZfHyh7ZAqsM4fNFUvhOJo0HFh0Wx3NrINy1aBUpFufhOz-s74d_haPPfKdA","expires_in":3600,"token_type":"Bearer"}]]
      token = json.parse{data=token}
      return token.access_token
   end

end

function queries.getExtCopyEmp(empID,token) 

   local uri = "https://vwdevsgbd01.logibec.com:8443/api/Employees/externalCopy/" .. empID
   --Not needed, but we'll keep it around in case
   --local body = json.serialize{data=recordIn}
   local headers = {['Authorization']='Bearer ' .. token}
   local method = 'get'
   
   trace(headers)

   local status, response = pcall(wsQuery,method,uri,headers)


   trace(status)
   trace(response)


   if status then
      if status ~= '' then return response else return '' end
   
   -- This condition is for my testing environment where I cannot reach the actual microservice
   else 
      local testResponse = ''--[[{
   "Version":"1",
   "Id":"999999",
   "Code":"999999",
   "firstName":"Casey",
   "middleName":"C",
   "lastName":"Trauer",
   "gender":"0",
   "hireDt":"04-APR-86",
   "terminationDt":"",
   "salutationId":"1",
   "employeeCategoryId":"-1"
}
      ]]
      
      
      return testResponse
   end

end


function queries.getExtCopyPhone(phoneID) 

   local uri = "https://vwdevsgbd01.logibec.com:8443/api/PhoneNumbers/externalCopy/" .. phoneID
   --Not needed, but we'll keep it around in case
   --local body = json.serialize{data=recordIn}
   local headers = {['Authorization']='Bearer ' .. token}
   local method = 'get'
   
   trace(headers)

   local status, response = pcall(wsQuery,method,uri,headers)

   trace(status)
   trace(response)


   if status then
      if status ~= '' then return response else return '' end
   
   -- This condition is for my testing environment where I cannot reach the actual microservice
   else 
      local testResponse = ''--[[{
"extension": null,
"employeeId": "000168",
"customLabel": null,
"number": "9059794646",
"label": "0",
"Id": "0001680",
"Version": 1
}
      ]]
      
      
      return testResponse
   end


end

function wsQuery(method,uri,headers,body) 

   if method == 'post' then
      local response, status, headers = net.http.post{url=uri,headers=headers,body=body,live=true}
      return response, status, headers

   elseif method == 'get' then
      local response, status, headers = net.http.get{url=uri,headers=headers,live=true}
      return response, status, headers

   end
end



-- Create the a table for customized help
local addHelp = {
       Title="Get external copy of employee record.";
       Usage="queries.getExtCopyEmp(empID,token)";
       Desc="Queries web service for an external copy of employee record.";
       Returns= {
          {Desc="A JSON string for external employee record or empty if no record is found."}
       };
       ParameterTable= false,
       Parameters= {
           {empID= {Desc="The employee record id from the database."; Opt=false}},
           {token= {Desc="The token string from the IDS."; Opt=false}},
       };
       Examples={
           [[local extCopyEmp = vip.ws.queries.getExtCopyEmp(empRecord.Id,token)
      
  RETURNS:    
      {
   "Version":"1",
   "Id":"",
   "Code":"",
   "firstName":"",
   "middleName":"",
   "lastName":"",
   "gender":"",
   "hireDt":"",
   "terminationDt":"null",
   "salutationId":"",
   "employeeCategoryId":""
}]]     
       };
       SeeAlso={
           {
               Title="Here's a link to external documentation.",
               Link="http://mydomain/my_extra_help.html"
           }
       }
   }
 
-- call the custom help functions for autocompletion support
help.set{input_function=queries.getExtCopyEmp,help_data=addHelp}


return queries
