local queries = {}

function queries.getQueue() 
local sql = [[SELECT DATETIME, CAST(CORRELATION_ID as VARCHAR2(2000)) as CORRELATION_ID, DTONAME, SOURCEID FROM HRPINTERFACEQUEUE]]

return sql
end


function queries.getEmpRecord(sourceID) 
local sql = [[select '{' 
||'"Type":"'||HRPQ.DTONAME||'"'  
||',"correlationId":"'||CORRELATION_ID||'"' 
||',"Version":"'||'1'||'"' -- Fixed Value always 1 
||',"Id":"'||I.INDV_ID||'"'
||',"Code":"'||I.INDV_ID||'"'
||',"firstName":"'||I.FRNAME||'"'
||',"middleName":"'||I.INIT||'"'
||',"lastName":"'||I.SRNAME||'"'
||',"gender":"'||case I.GEND_CD when 'M' then 0 when 'F' then 1 else 2 end ||'"'
||',"hireDt":"'||'2007-05-05'||'"'  --default missing constraint table
||',"terminationDt":'||'null'||''
||',"salutationId":"'||I.SALUT_CD||'"'
||',"employeeCategoryId":"'||'-1'||'"'  --default missing constraint
||',"salutationId":"'||I.SALUT_CD||'"'

   ||'} ' json
from INDV_T I
join HrpInterfaceQueue HRPQ on HRPQ.DTONAME = 'Employee'
where I.INDV_ID = ]] .. sourceID .. [[
 and rownum = 1]]

return sql
end


function queries.getEmpPhoneHome(sourceID)
local sql = [[select '{'
||'"Type":"'||'EmployeePhone'||'"'
||',"correlationId":"'||CORRELATION_ID||'"'
||',"Version":"'||'1'||'"'
||',"Id":"'||CONCAT(I.INDV_ID,0)||'"'
||',"employeeId":"'||I.INDV_ID||'"'
||',"label":"'||0||'"'
||',"customLabel":'||'null'||''
||',"number":"'||CONCAT(I.AREA_CD1,TEL1)||'"'
||',"extension":'||'null'||''
||'} ' json
from INDV_T I
join HrpInterfaceQueue HRPQ on HRPQ.DTONAME = 'Employee'
where I.INDV_ID = ]] .. sourceID .. [[
 and rownum = 1]]

return sql
end

function queries.getEmpPhoneMobile(sourceID)
local sql = [[select '{'
||'"Type":"'||'EmployeePhone'||'"'
||',"correlationId":"'||CORRELATION_ID||'"'
||',"Version":"'||'1'||'"'
||',"Id":"'||CONCAT(I.INDV_ID,1)||'"'
||',"employeeId":"'||I.INDV_ID||'"'
||',"label":"'||1||'"'
||',"customLabel":'||'null'||''
||',"number":"'||CONCAT(I.AREA_CD2,TEL2)||'"'
||',"extension":'||'null'||''
||'} ' json
from INDV_T I
join HrpInterfaceQueue HRPQ on HRPQ.DTONAME = 'Employee'
where I.INDV_ID = ]] .. sourceID .. [[
 and rownum = 1]]

return sql
end

return queries