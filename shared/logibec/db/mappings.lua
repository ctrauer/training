local mappings = {}

-- Defines how we will send record from From Translator component to Filter component.
function mappings.recordOut() 

   out = [[
   {'datetime':'',
   'correlation_id':'',
   'dtoname':'',
   'sourceid':''}
   ]]

   return json.parse(out)
   
end

function mappings.mapRecordOut(record,recordOut) 

   recordOut.datetime = record.DATETIME:nodeValue()
   recordOut.correlation_id = record.CORRELATION_ID:nodeValue()
   recordOut.dtoname = record.DTONAME:nodeValue()
   recordOut.sourceid = record.SOURCEID:nodeValue()

   return recordOut
end

return mappings