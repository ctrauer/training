local map = {}

-- Define the outgoing message format. In this case, it's JSON.
function map.template() 
   local JSONTemplate = [[{"patient": {
   "id": "",
   "name_first": "",
   "name_last": "",
   "city": "",
   "state": "",
   "zip": "",
   "phone_home": "",
   "phone_work": "",
   "race": "", 
   "gender": "", 
   "ssn": "" 
}
}]]
   
   return JSONTemplate
end

function map.buildOutboundMsg(MsgIn,MsgOut,mappings) 
   
   -- Parse MsgOut
   local MsgOut = json.parse{data=MsgOut}
   
   -- Extract PID data and map to MsgOut
   mappings.mapPID(MsgIn.PID,MsgOut)
   return MsgOut
   
end



return map