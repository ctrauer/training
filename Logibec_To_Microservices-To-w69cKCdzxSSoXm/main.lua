-- The main function is the first function called from Iguana.
-- The Data argument will contain the message to be processed.

local vip = {}
local queries = require 'logibec.ws.queries'
vip.queries = queries


function main(Data)
   
   local RecordIn = json.parse{data=Data}
   local command = RecordIn.commands[1].type
  
   local token = vip.queries.getToken()
   
   
   local uri

   local headers = {['Authorization']='Bearer ' .. token,['Content-Type']='application/json'}
   local method = 'post'

   if command == 'CreateEmployee' then
      trace(true)
      uri = 'https://vwdevsgbd01.logibec.com:8443/api/Employees/BatchedCommands'
      
      elseif command == 'ModifyEmployee' then
      trace(true)
      uri = 'https://vwdevsgbd01.logibec.com:8443/api/Employees/BatchedCommands'
      
      elseif command == 'CreatePhoneNumber' then
      trace(true)
      uri = 'https://vwdevsgbd01.logibec.com:8443/api/PhoneNumbers/BatchedCommands'
      
      elseif command == 'ModifyPhoneNumber' then
      trace(true)
      uri = 'https://vwdevsgbd01.logibec.com:8443/api/PhoneNumbers/BatchedCommands'
      
      elseif command == 'DeletePhoneNumber' then
      trace(true)
      uri = 'https://vwdevsgbd01.logibec.com:8443/api/PhoneNumbers/BatchedCommands'

      else
      iguana.logError('No commands matched.')
      end
      
      local response, status = pcall(wsQuery,method,uri,headers,Data)
   

   if status then 
      iguana.logInfo('Event message ' .. command .. ' for record ' .. RecordIn.commands[1].data.Id  .. ' submitted to API.'   )   
else
      iguana.logInfo('Could not connect to webservice at ' .. uri)
   end
      
end

function wsQuery(method,uri,headers,body) 

   if method == 'post' then
      local response, status, headers = net.http.post{url=uri,headers=headers,body=body,live=true}
      
      iguana.logInfo(status)
      return response, status, headers
      

   elseif method == 'get' then
      local response, status, headers = net.http.get{url=uri,headers=headers,live=true}
      iguana.logInfo(status)
      return response, status, headers

   end
end
