-- The main function is the first function called from Iguana.
-- The Data argument will contain the message to be processed.

require 'cleanphone'
require 'dateparse'
require 'split'
require 'mappings'
hl7.parseZsegment = require 'hl7.zsegment'
hl7.copyZsegment = require 'hl7.zsegment_copy'

local function trace(A) return end

function main(Data)

   -- #PARSE INCOMING FILE
   local Orig, Name, Errors = hl7.parse{vmd="demo.vmd",data=Data}


   
   

   
   -- #FILTER FILES
   if Orig:nodeName() == 'Catchall' then 
      iguana.logInfo('This message was filtered '..Orig.MSH[9][1]..' '..Orig.MSH[9][2]) 
      return
   end

  
   
   -- #CREATE OUTBOUND FILES
   local Out = hl7.message{vmd='demo.vmd', name=Orig:nodeName()}
   
   
   -- #LOG VIP PATIENTS FOR ALERT
   CheckForVIP(Orig)
   
   -- #MAP INBOUND TO OUTBOUND FILE
   Out:mapTree(Orig)

   -- For Demo, remap surname



   -- #TRANSFORM OUTBOUND FILE FOR EACH MESSAGE TYPE
   AlterMSH(Out.MSH)
   if Orig:nodeName() == 'Lab' then 
      AlterPID(Out.PATIENT.PID)
   elseif Orig:nodeName() == 'ADT' then 
      AlterPID(Out.PID)
   end
   
   if Out:nodeName() == 'Lab' then
      AddNote(Out)  
   end
   
   trace(Out)

   local DataOut = tostring(Out)
   

   
   queue.push{data=DataOut}

end


