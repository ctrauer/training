-- The main function is the first function called from Iguana.

local batch = {}
batch.split = require 'hl7.batch'

-- Convert \r\n and \n into standard for HL7 \r, if any
local function ConvertTerminators(Data)
   Data = Data:gsub('\r\n','\r')
   return Data:gsub('\n','\r')
end

function main()

   -- ## LOCATE BATCH FILE
   
   -- Example 1: Hardcode the path
   -- local tFolder = ‘/Users/ctrauer/Documents/Iguana6Data/Training/sampledata’
   local tFolder = iguana.workingDir() .. 'Training/sampledata/'
   trace(tFolder)
   local aFolder = iguana.workingDir() .. 'Traning/sampledata_archive/'
   
   
   local tFile = 'sample_data.txt'

   -- Example 2: Use environmental variables
   --local tFolder = os.getenv('tFolder')

   
   -- ## IMPORT BATCH FILE

   local F = io.open(tFolder .. tFile, 'r')
   
   local Data = F:read("*a")
   F:close()

   
   -- ## PROCESS/SPLIT BATCH FILE
   
   -- convert any non-HL7-standard terminators
   Data = ConvertTerminators(Data)
   
   -- split HL7 messages into table rows
   Messages = batch.split(Data)
   trace(Messages)

   
   
   -- ## SEND MESSAGES TO QUEUE
   
   for i=1, #Messages do
      queue.push{data=Messages[i]}   
   end  
   
   
   -- ## MOVE FILE TO ARCHIVE

   local oldFile = tFolder .. tFile
   local newFile = tFolder .. os.date('%Y%m%d%H%M%S') .. tFile
   trace (oldFile, newFile)
   
   os.rename(oldFile, newFile)

end