-- Import store module
local store2 = require 'store2'

-- Create table of functions
local counter = {}

-- Add one to the counter for this channel
function counter.increment() 

   -- Connect to database using project GUID or channelGuid
   local LocalChannelStore = store2.connect('Training/' .. iguana.channelGuid())
   trace(LocalChannelStore)

   -- Get current value
   local count = LocalChannelStore:get("count")

   -- Create a variable to store the new value
   local newcount

   -- Add value to counter, but only if running from the Dashboard
   if iguana.isTest() == false then
      if count == nil then 
         newcount = 1
      else
         newcount = count + 1   
      end
      trace(newcount)

      -- Store new value  
      LocalChannelStore:put("count",newcount)

   end

   -- Browse database
   LocalChannelStore:info()
end

-- Return table of functions to main script
return counter