-- The main function is the first function called from Iguana.

-- ## Add required modules needed to support interface
local vip = require 'logibec.db.connect'
local queries = require 'logibec.db.queries'
local mappings = require 'logibec.db.mappings'
-- Extend VIP namespace to add more functions. Useful for popup menus!
vip.db = {}
vip.db.queries = queries
vip.db.mappings = mappings

-- ## Define database connection objectx
local conn = vip.init()


function main()
   
-- ## Query queue table in database
   local records = conn:query{sql=vip.db.queries.getQueue()}

   
-- ## Convert each record to JSON string and push to the queue
   
   -- loops through all records in result set
   for i=1, #records do 
	
      -- generates JSON template 
      local recordOut = vip.db.mappings.recordOut()

      -- maps db record values to JSON outbound message 
      vip.db.mappings.mapRecordOut(records[i],recordOut)

      -- queue JSON message (ensuring we serialize to a string)
      queue.push{data=json.serialize{data=recordOut}}
   
   end
   
end